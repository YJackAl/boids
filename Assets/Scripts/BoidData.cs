﻿using UnityEngine;

public struct GPUBoidData
{
    public Vector3 position;
    public Vector3 direction;
    public float noise_offset;


    public GPUBoidData(Vector3 position, Vector3 direction, float noise_offset = 0)
    {
        this.position = position;
        this.direction = direction;
        this.noise_offset = noise_offset;
    }
}