﻿using UnityEngine;


public class BoidGenerator : MonoBehaviour
{
    public string kernelName = "CSMain";
    public ComputeShader computeShaderBoid;

    public int numberOfBoids = 10000;
    public float SpawnRadius = 100;
    public GPUBoidData[] boidsData;
    [HideInInspector] public ForceFieldData[] forceFieldData;

    public Transform Target;

    private ForceField[] forceFields;
    private int kernelHandle;
    private ComputeBuffer BoidBuffer;
    private ComputeBuffer ForceFieldBuffer;
    private GPURenderer gpuRenderer;
    const int GROUP_SIZE = 512;

    private void Awake()
    {
        gpuRenderer = GetComponent<GPURenderer>();
        gpuRenderer.numberOfMeshes = numberOfBoids;
    }

    void Start()
    {
        kernelHandle = computeShaderBoid.FindKernel(kernelName);

        boidsData = new GPUBoidData[numberOfBoids];
        for (int i = 0; i < numberOfBoids; i++)
            boidsData[i] = new GPUBoidData(transform.position + Random.insideUnitSphere * SpawnRadius, Random.rotation.eulerAngles, Random.value * 1000.0f);

        BoidBuffer = new ComputeBuffer(numberOfBoids, System.Runtime.InteropServices.Marshal.SizeOf<GPUBoidData>());
        BoidBuffer.SetData(boidsData);

        forceFields = FindObjectsOfType<ForceField>();
        forceFieldData = new ForceFieldData[forceFields.Length];
        for (int i = 0; i < forceFields.Length; i++)
            forceFieldData[i] = forceFields[i].forceFieldData;

        ForceFieldBuffer = new ComputeBuffer(forceFieldData.Length, System.Runtime.InteropServices.Marshal.SizeOf<ForceFieldData>());
        ForceFieldBuffer.SetData(forceFieldData);
    }

    public float RotationSpeed = 1f;
    public float BoidSpeed = 1f;
    public float NeighbourDistance = 10f;
    public float BoidSensorDistance = 1f;
    public float BoidSpeedVariation = 1f;
    void Update()
    {
        computeShaderBoid.SetFloat("DeltaTime", Time.deltaTime);
        computeShaderBoid.SetFloat("RotationSpeed", RotationSpeed);
        computeShaderBoid.SetFloat("BoidSpeed", BoidSpeed);
        computeShaderBoid.SetFloat("BoidSpeedVariation", BoidSpeedVariation);
        computeShaderBoid.SetVector("FlockPosition", Target.transform.position);
        computeShaderBoid.SetFloat("NeighbourDistance", NeighbourDistance);
        computeShaderBoid.SetFloat("BoidSensorDistance", BoidSensorDistance);
        computeShaderBoid.SetInt("BoidsCount", numberOfBoids);
        computeShaderBoid.SetBuffer(kernelHandle, "boidBuffer", BoidBuffer);

        for (int i = 0; i < forceFields.Length; i++)
            forceFieldData[i] = forceFields[i].forceFieldData;
        ForceFieldBuffer.SetData(forceFieldData);
        computeShaderBoid.SetBuffer(kernelHandle, "forceFieldBuffer", ForceFieldBuffer);

        computeShaderBoid.Dispatch(kernelHandle, this.numberOfBoids / GROUP_SIZE + 1, 1, 1);

        gpuRenderer.Render(BoidBuffer);
    }

    void OnDestroy()
    {
        if (BoidBuffer != null) 
            BoidBuffer.Release();

        if (ForceFieldBuffer != null)
            ForceFieldBuffer.Release();
    }
}
