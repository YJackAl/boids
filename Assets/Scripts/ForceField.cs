﻿using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class ForceField : MonoBehaviour
{
    public ForceFieldData forceFieldData;

    private SphereCollider sphereCollider;
    private void Awake()
    {
        sphereCollider = GetComponent<SphereCollider>();
    }
    private void Update()
    {
        UpdateStruct();
    }

    private void UpdateStruct()
    {
        forceFieldData.position = transform.position;
        forceFieldData.radius = sphereCollider.radius;
    }

    private void OnDrawGizmos()
    {
        float colorAtten = ((forceFieldData.force / 10) + 0.5f); //0 to 1 domain
        Gizmos.color = Color.Lerp(Color.red, Color.blue, colorAtten);
        Gizmos.DrawWireSphere(forceFieldData.position, forceFieldData.radius);
    }
}

[System.Serializable]
public struct ForceFieldData
{
    [HideInInspector] public Vector3 position;
    [Range(-5, 5)] public float force;
    [HideInInspector] public float radius;

    public ForceFieldData(Vector3 position, float force, float radius)
    {
        this.position = position;
        this.force = force;
        this.radius = radius;
    }
}