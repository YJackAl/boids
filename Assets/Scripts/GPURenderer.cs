﻿using UnityEngine;

public class GPURenderer : MonoBehaviour
{
    public int numberOfMeshes = 10000;
    public Mesh mesh;
    public Material material;

    ComputeBuffer drawBuffer;
    MaterialPropertyBlock _props;

    void Start()
    {
        // Initialize the indirect draw args buffer.
        drawBuffer = new ComputeBuffer(1, 5 * sizeof(uint), ComputeBufferType.IndirectArguments);

        // This property block is used only for avoiding an instancing bug.
        _props = new MaterialPropertyBlock();
        _props.SetFloat("_UniqueID", Random.value);

        uint[] drawData = new uint[5] { mesh.GetIndexCount(0), (uint)numberOfMeshes, 0, 0, 0 };
        drawBuffer.SetData(drawData);
    }

    public void Render(ComputeBuffer buffer)
    {
        material.SetBuffer("boidBuffer", buffer);
        Graphics.DrawMeshInstancedIndirect(mesh, 0, material, new Bounds(Vector3.zero, Vector3.one * 1000), drawBuffer, 0, _props);
    }

    void OnDestroy()
    {
        if (drawBuffer != null)
           drawBuffer.Release();
    }
}
